#!/bin/bash

source .env.local
#git config --global --add url."git@gitlab.com:".insteadOf "https://gitlab.com/"

mkdir -p $USERS_SERVICE_DIR
git clone git@gitlab.com:ms-example/users-service.git $USERS_SERVICE_DIR

mkdir -p $DOCS_DIR
git clone git@gitlab.com:ms-example/docs.git $DOCS_DIR
