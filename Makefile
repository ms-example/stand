PROJECT_NAME = ms-example
ENV_FILE = .env.local

init: env clone
	@echo "init stand"

env:
	cp -n .env .env.local

clone:
	chmod +x scripts/clone.sh
	scripts/clone.sh

pull:
	chmod +x scripts/pull.sh
	scripts/pull.sh

hosts:
	echo "127.0.0.1\tkafka,zookeeper,postgres,prometheus,jaeger,grafana,kafkaui" >> /etc/hosts
	echo "127.0.0.1\tpublic.grpc.users.localhost,internal.grpc.users.localhost" >> /etc/hosts

up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d

stop:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) stop

down:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) down --remove-orphans

kafka.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build kafka

kafka.ui.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build kafka_ui

zookeeper.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build zookeeper

postgres.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build postgres

jaeger.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build jaeger

prometheus.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build prometheus

grafana.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build grafana

docs.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build docs

users.up:
	@docker-compose --project-name $(PROJECT_NAME) --env-file $(ENV_FILE) up -d --build users_service
	@echo "\n"
	@echo "http://users.localhost:8001/metrics"
	@echo "grpc://public.grpc.users.localhost:9100"
	@echo "grpc://internal.grpc.users.localhost:9101"
	@echo "\n"
